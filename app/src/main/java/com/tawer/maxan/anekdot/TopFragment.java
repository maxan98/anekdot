package com.tawer.maxan.anekdot;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.Space;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;

import java.util.ArrayList;
import java.util.List;


public class TopFragment extends Fragment {
DBHelper dbHelper;
    View rootview;
    ImageView imageView;
    private List<EditText> editTextList = new ArrayList<EditText>();
    LinearLayout li;
    Bitmap bitmap;
    Bitmap mutableBitmap;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        rootview = inflater.inflate(R.layout.fragment_top, null);
        dbHelper = new DBHelper(getContext());
        li = rootview.findViewById(R.id.ll);
        bitmap = BitmapFactory.decodeResource(rootview.getResources(),R.drawable.line);
        mutableBitmap = bitmap.copy(Bitmap.Config.ARGB_8888, true);
        mutableBitmap.setHeight(10);
        Log.d("qqwer",mutableBitmap.getHeight()+"");
        getDB("best",10);




        return rootview;

    }



    public class DBHelper extends SQLiteOpenHelper {

        public DBHelper(Context context) {
            // конструктор суперкласса
            super(context, "myDB", null, 1);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            Log.d("DBMY", "--- onCreate database ---");
            // создаем таблицу с полями
            db.execSQL("create table mytable ("
                    + "id integer primary key autoincrement,"
                    + "category text,"
                    + "data text"+ "likes integer" + ");");
        }


        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }
    }


    protected void parsejson(String str) throws IOException {

        SQLiteDatabase db = dbHelper.getWritableDatabase();

        // делаем запрос всех данных из таблицы mytable, получаем Cursor
        Cursor c = db.query("mytable", null, null, null, null, null, null);

        // ставим позицию курсора на первую строку выборки
        // если в выборке нет строк, вернется false
        if (c.moveToFirst()) {

            // определяем номера столбцов по имени в выборке
            int idColIndex = c.getColumnIndex("id");
            int nameColIndex = c.getColumnIndex("data");
            int emailColIndex = c.getColumnIndex("category");

            do {
                // получаем значения по номерам столбцов и пишем все в лог
                Log.d("DBMY",
                        "ID = " + c.getInt(idColIndex) +
                                ", data = " + c.getString(nameColIndex) +
                                ", category = " + c.getString(emailColIndex));
                // переход на следующую строку
                // а если следующей нет (текущая - последняя), то false - выходим из цикла
            } while (c.moveToNext());
        } else
            Log.d("DBMY", "0 rows");
        c.close();
        dbHelper.close();


    }
    public void getDB(String category, int number){
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor c = db.query("mytable",null,null,null,null,null,null);
        int i = 0;
        if(c.moveToFirst()){
            int id = c.getColumnIndex("id");
            int data = c.getColumnIndex("data");
            int cat = c.getColumnIndex("category");
            do {
                if(c.getString(cat).equals(category)){

                    EditText  editTxt = new EditText(getContext());
                    editTxt.setText(c.getString(data));
                    editTxt.setRawInputType(0x0000000000);
                    editTextList.add(i, editTxt);
                    ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT);

                    imageView = new ImageView(getContext());
                    imageView.setImageBitmap(bitmap);
                    imageView.setMaxHeight(10);
                    imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                    imageView.setAdjustViewBounds(true);
                    imageView.setBackgroundColor(rootview.getResources().getColor(R.color.text_space));
                   // imageView.setLayoutParams(lp);
                    //listView.addView(editTxt);
                    editTxt.setBackgroundColor(rootview.getResources().getColor(R.color.text_container));
                    li.addView(editTxt);
                    li.addView(imageView);
                   // listView.addView(imageView);

                }
                i++;
            } while (c.moveToNext()&& i < number);

        } else Log.d("DBMY","TROUBLES");
    }

}
